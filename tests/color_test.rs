#[cfg(test)]
mod tests {
    use hex_color::HexColor;
    use random_color::RandomColor;
    use scythe_ci_demo::color::{hex_to_rgb, RGB};

    #[test]
    fn test_hex_string_too_short() {
        assert!(hex_to_rgb("00000").is_err())
    }

    #[test]
    fn test_hex_string_too_long() {
        assert!(hex_to_rgb("0000000").is_err())
    }

    #[test]
    fn test_hex_string_invalid_hex() {
        assert!(hex_to_rgb("colors").is_err())
    }

    #[test]
    fn test_random_colors() {
        for _ in 0..12 {
            let color_hex = RandomColor::new().to_hex();
            let color_rgb = HexColor::parse(&color_hex).unwrap();

            println!(
                "Parsing hex {} - expected RGB representation is: rgb({},{},{})",
                color_hex, color_rgb.r, color_rgb.g, color_rgb.b
            );

            let parsed_hex = hex_to_rgb(&color_hex).unwrap();
            println!(
                "Output representation was: rgb({},{},{})",
                parsed_hex.r, parsed_hex.g, parsed_hex.b
            );

            assert_eq!(
                parsed_hex,
                RGB {
                    r: color_rgb.r,
                    g: color_rgb.g,
                    b: color_rgb.b,
                }
            )
        }
    }
}
