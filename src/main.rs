use std::env;
use std::process;

use scythe_ci_demo::color::hex_to_rgb;

fn main() {
    let hex_code_arg = env::args().nth(1);

    if let Some(hex_code) = hex_code_arg{
        match hex_to_rgb(&hex_code) {
            Ok(rgb) => {
                println!(
                    "The hex color code {} has the following RGB representation: rgb({},{},{})",
                    hex_code, rgb.r, rgb.g, rgb.b
                );
            }
            Err(error) => {
                eprintln!("Failed to convert hex to RGB: {}", error);
                process::exit(-2);
            }
        }
    } else {
        eprintln!("No hex color code was provided, nothing to do!");
        process::exit(-1);
    }
}
