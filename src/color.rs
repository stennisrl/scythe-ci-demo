use anyhow;

#[derive(Debug, PartialEq)]
pub struct RGB {
    pub r: u8,
    pub g: u8,
    pub b: u8,
}

//Just a quick implementation that only accepts the 6-value hex color syntax.
pub fn hex_to_rgb(hex_code: &str) -> anyhow::Result<RGB> {
    let raw_hex_code = if let Some(stripped_hex_code) = hex_code.strip_prefix('#'){
        stripped_hex_code
    } else {
        hex_code
    };

    if raw_hex_code.len() < 6 || raw_hex_code.len() > 6{
        anyhow::bail!("Malformed hex color code!: {}", hex_code);
    }

    let hex_value = u32::from_str_radix(raw_hex_code, 16)?;

    Ok(RGB{
        r: (((hex_value >> 16) & 0xFF)) as u8,
        g: (((hex_value >> 8) & 0xFF)) as u8,
        b: ((hex_value & 0xFF)) as u8,
    })
}
