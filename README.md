# Scythe CI Demo

This is a minimal demo completed as part of the Scythe Robotics take-home assignment. The included demo application takes a 6-value hex color code, and converts it to RGB. 

The CI/CD pipeline will automatically build and test the demo application, then publish it to GitLab as an artifact.

## Building
Firstly, ensure you have the latest version of Rust installed on your computer: https://www.rust-lang.org/tools/install

From the root directory, run `cargo build` or `cargo build -r` depending on if you want the debug or release binaries.
Tests can be invoked using `cargo test`.

## Usage
The application expects a single argument, a hex color code provided as a string.
```console
$ scythe_ci_demo "#009efc"

The hex color code #009efc has the following RGB representation: rgb(0,158,252)
```
